# Graphical Assets

Graphical assets for DANIO-CODE. The logo is based on an idea by Julia Horsfield, extended by Matthias Hörtenhuber.

⚠️ Please use the `transparent-bg` versions only on a non-white background, otherwise the white squiggle is not visible. ⚠️
# Vector format

[danio-code_blue-bg.pdf](danio-code_blue-bg.pdf)

[danio-code_no-text_blue-bg.pdf](danio-code_no-text_blue-bg.pdf)

[danio-code_square_blue-bg.pdf](danio-code_square_blue-bg.pdf)

[danio-code_transparent-bg.pdf](danio-code_transparent-bg.pdf)

[danio-code_no-text_transparent-bg.pdf](danio-code_no-text_transparent-bg.pdf)

[danio-code_square_transparent-bg.pdf](danio-code_square_transparent-bg.pdf)

# Rasterized format

![danio-code_blue-bg.png](danio-code_blue-bg.png)

![danio-code_no-text_blue-bg.png](danio-code_no-text_blue-bg.png)

![danio-code_square_blue-bg.png](danio-code_square_blue-bg.png)

![danio-code_transparent-bg.png](danio-code_transparent-bg.png)

![danio-code_no-text_transparent-bg.png](danio-code_no-text_transparent-bg.png)

![danio-code_square_transparent-bg.png](danio-code_square_transparent-bg.png)

# Source file

[danioc-code_logo.afdesign](danioc-code_logo.afdesign)